import tensorflow as tf

input_dim = 4;
hidden_dim = 10;
output_dim = 1;

# initialize place holders for model
x = tf.placeholder(tf.float32, [None, input_dim]);
y_ = tf.placeholder(tf.float32, [None,output_dim]);

#initalize weights for model
W1 = tf.Variable(tf.truncated_normal([input_dim, hidden_dim], stddev=0.1));
b1 = tf.Variable(tf.zeros([hidden_dim]));
W2 = tf.Variable(tf.truncated_normal([hidden_dim, output_dim], stddev=0.1));
b2 = tf.Variable(tf.zeros([output_dim]));

#make model
y = tf.matmul(tf.nn.relu(tf.matmul(x,W1) + b1), W2) + b2

#Make loss function and gradient operations
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
loss = tf.reduce_sum(tf.square(y_-y));
#optimizer = tf.train.GradientDescentOptimizer(0.05)
optimizer = tf.train.AdamOptimizer()
grads_and_vars = optimizer.compute_gradients(loss);
grad_placeholder = [(tf.placeholder(tf.float32, shape=grad[0].get_shape()), grad[1]) for grad in grads_and_vars]
apply_placeholder_op = optimizer.apply_gradients(grad_placeholder)

sess = tf.Session();
init = tf.global_variables_initializer();
sess.run(init);
print(sess.run(y, {x:[[1,1,1,1]]}));

for _ in range(1000):
    grad_vals = sess.run(grads_and_vars, {x:[[1,1,1,1]], y_:[[1]]});

    feed_dict = {}
    for i in range(len(grad_placeholder)):
        feed_dict[grad_placeholder[i][0]] = grad_vals[i][0]

    sess.run(apply_placeholder_op, feed_dict=feed_dict)


    print(sess.run(y, {x:[[1,1,1,1]]}));
