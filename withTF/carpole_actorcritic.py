import gym
import tensorflow as tf
import random
import numpy as np
import matplotlib
matplotlib.use('TKAgg');
import matplotlib.pyplot as plt

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

learning_rate = 1e-4;
gamma = 0.95;

input_dim = 4;
hidden_dim = 300;
output_dim = 1;

"""
   Makes the policy network
"""
# initialize place holders for model
x = tf.placeholder(tf.float32, [None, input_dim]);
y_ = tf.placeholder(tf.float32, [None, output_dim]);

#initalize weights for model
W1 = tf.Variable(tf.truncated_normal([input_dim, hidden_dim], stddev=0.1));
b1 = tf.Variable(tf.zeros([hidden_dim]));
W2 = tf.Variable(tf.truncated_normal([hidden_dim, output_dim], stddev=0.1));
b2 = tf.Variable(tf.zeros([output_dim]));

#make model
y = tf.sigmoid(tf.matmul(tf.nn.relu(tf.matmul(x,W1) + b1), W2) + b2);

loss = tf.reduce_sum(tf.square(y_-y));
#cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y));

#optimizer = tf.train.GradientDescentOptimizer(learning_rate)
optimizer = tf.train.AdamOptimizer()
grads_and_vars = optimizer.compute_gradients(loss, var_list=[W1, b1, W2, b2]);
grad_placeholder = [(tf.placeholder(tf.float32, shape=grad[0].get_shape()), grad[1]) for grad in grads_and_vars]
apply_placeholder_op = optimizer.apply_gradients(grad_placeholder)



"""
    Makes the baseline network
"""
# initialize place holders for model
x_state = tf.placeholder(tf.float32, [None, input_dim]);
y_state_ = tf.placeholder(tf.float32, [None, output_dim]);

#initalize weights for model
W1_state = tf.Variable(tf.truncated_normal([input_dim, hidden_dim], stddev=0.1));
b1_state = tf.Variable(tf.zeros([hidden_dim]));
W2_state = tf.Variable(tf.truncated_normal([hidden_dim, output_dim], stddev=0.1));
b2_state = tf.Variable(tf.zeros([output_dim]));

#make model
y_state = tf.matmul(tf.nn.relu(tf.matmul(x_state,W1_state) + b1_state), W2_state) + b2_state;
#y_state = tf.matmul(tf.sigmoid(tf.matmul(x_state,W1_state) + b1_state), W2_state) + b2_state;

loss_state = tf.reduce_sum(tf.square(y_state_ - y_state));
#cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y));

#optimizer_state = tf.train.GradientDescentOptimizer(learning_rate)
optimizer_state = tf.train.AdamOptimizer()
grads_and_vars_state = optimizer_state.compute_gradients(loss_state, var_list=[W1_state, b1_state, W2_state, b2_state]);
grad_placeholder_state = [(tf.placeholder(tf.float32, shape=grad[0].get_shape()), grad[1]) for grad in grads_and_vars_state]
apply_placeholder_op_state = optimizer_state.apply_gradients(grad_placeholder_state)



sess = tf.Session();
init = tf.global_variables_initializer();
sess.run(init);

env = gym.make('CartPole-v0');

average_reward = 0;
for i in range(300):
    I = 1.0;
    observation = env.reset();
    
    new_obs = observation;
    for t in range(202):
        #env.render()
        
        prob = sess.run(y, {x:[observation]});
        value = sess.run(y_state, {x_state:[observation]})[0][0];
        
        action = 1 if prob > random.random() else 0;
        
        new_obs, reward, done, info = env.step(action);
        
        if (done):
            next_value = 0;
        else:
            next_value = sess.run(y_state, {x_state:[new_obs]})[0][0];
        
        
        delta = reward + gamma * next_value - value;
        
        grad_vals = sess.run(grads_and_vars, {x:[observation], y_:[[action]]});
        feed_dict = {}
        for k in range(len(grad_placeholder)):
            feed_dict[grad_placeholder[k][0]] = delta * I * grad_vals[k][0];
        sess.run(apply_placeholder_op, feed_dict=feed_dict)

        for _ in range(1):
            grad_vals_state = sess.run(grads_and_vars_state, {x_state:[observation], y_state_:[[reward+gamma*next_value]]});
            feed_dict = {}
            for k in range(len(grad_placeholder_state)):
                feed_dict[grad_placeholder_state[k][0]] = grad_vals_state[k][0];
            sess.run(apply_placeholder_op_state, feed_dict=feed_dict)

        observation = new_obs;
        I = I * gamma;


        if (i == 290):
            foo = sess.run(y_state, {x_state:[observation]})[0][0];
            print("Iteration {0}, step {1},\t Goal {2:0.3f}, \t new {6:0.3f} \t old {3:0.3f},\t error {4:0.3f},\t change {5:0.3f}".format(i,t,reward + gamma * next_value, value, reward+gamma*next_value - value, foo - value, foo));

        if (done):
            average_reward = 0.9*average_reward + 0.1 * t;
            if (i % 10 == 0):
                print("Episode {2} finished after {0} timesteps. Average reward {1}".format(t+1, average_reward, i))
        
            break

