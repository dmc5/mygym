import gym
import tensorflow as tf
import random
import numpy as np

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

learning_rate = 1e-2;
gamma = 0.95;

input_dim = 4;
hidden_dim = 100;
output_dim = 1;

# initialize place holders for model
x = tf.placeholder(tf.float32, [None, input_dim]);
y_ = tf.placeholder(tf.float32, [None,output_dim]);

#initalize weights for model
W1 = tf.Variable(tf.truncated_normal([input_dim, hidden_dim], stddev=0.1));
b1 = tf.Variable(tf.zeros([hidden_dim]));
W2 = tf.Variable(tf.truncated_normal([hidden_dim, output_dim], stddev=0.1));
b2 = tf.Variable(tf.zeros([output_dim]));

#make model
y = tf.sigmoid(tf.matmul(tf.nn.relu(tf.matmul(x,W1) + b1), W2) + b2);

loss = tf.reduce_sum(tf.square(y_-y));
#cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y));

#optimizer = tf.train.GradientDescentOptimizer(learning_rate)
optimizer = tf.train.AdamOptimizer()
grads_and_vars = optimizer.compute_gradients(loss);
grad_placeholder = [(tf.placeholder(tf.float32, shape=grad[0].get_shape()), grad[1]) for grad in grads_and_vars]
apply_placeholder_op = optimizer.apply_gradients(grad_placeholder)

sess = tf.Session();
init = tf.global_variables_initializer();
sess.run(init);

env = gym.make('CartPole-v0');

average_reward = 0;
for i in range(10000):
    states = [];
    probs = [];
    hiddens = [];
    actions = [];
    rewards = [];
    observation = env.reset();
    states.append(observation);
    for t in range(1000):
        #env.render()
        
        prob = sess.run(y, {x:[observation]});
        action = 1 if prob > random.random() else 0;
        
        observation, reward, done, info = env.step(action);
        
        actions.append(action);
        probs.append(prob);
        rewards.append(reward);
        states.append(observation);
        
        if (done):
            feed_dict = {}
            average_reward = 0.9*average_reward + 0.1 * t;
            for step in range(t):
                g = sum(rewards[step:]);  #need to discount
                
                
                
                grad_vals = sess.run(grads_and_vars, {x:[states[step]], y_:[[actions[step]]]});
                
                
                for k in range(len(grad_placeholder)):
                    feed_dict[grad_placeholder[k][0]] = g * gamma ** step * grad_vals[k][0];
                
                sess.run(apply_placeholder_op, feed_dict=feed_dict)
                
            
            if (i % 10 == 0):
                print("Episode {2} finished after {0} timesteps. Average reward {1}".format(t+1, average_reward, i))
            break
