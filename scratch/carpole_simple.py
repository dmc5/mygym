import gym
import numpy as np
import random


barriers = [-2.4,-2.0 -1.0, 1.0, 2.0, 2.4];
barriersDot = [-0.5, -.01, .01, 0.5, float('inf')];
barriers2 = [-np.pi/15.0, -np.pi/30.0, -np.pi/45.0, -np.pi/180.0, np.pi/180.0, np.pi/45.0, np.pi/30, np.pi/15.0];
barriers2Dot = [-np.pi/45, -np.pi/90.0, -np.pi/180, np.pi/180.0, np.pi/90.0,np.pi/45.0, float('inf')];
STATES = (len(barriers)-1)*(len(barriers2)-1)*len(barriersDot)*len(barriers2Dot);


def classify(state):
    """
    Takes in the state (car position, car velocity, pole angle, pole angular velocity)
    and returns the state number of the system
    """
    num = 0;
        #if state[0] < barriers[0] or state[0] > barriers[-1] or state[2] < barriers2[0] or state[2] > barriers2[-1]:
        #    return -1;
    
    #Tile car position
    for i in range(len(barriers)-1):
       if state[0] < barriers[i+1]:
           num = i
           break;

    #Tile for car velocity
    for i in range(len(barriersDot)-1):
       if state[1] < barriersDot[i+1]:
           num += i*(len(barriers)-1);
           break;
    
    #Tile for pole angle
    for i in range(len(barriers2)):
        if state[2] < barriers2[i]:
            num += (i)*(len(barriersDot)-1)*(len(barriers)-1);
            break;

    #Tile for theta2dot
    for i in range(len(barriers2Dot)):
        if state[3] < barriers2Dot[i]:
            num += (i) * (len(barriersDot)) * (len(barriers2)-1)*(len(barriers)-1);
            break;
    
    return num

def policyProbability(state,weights):
    actions = [0,1];
    actionPreference = np.zeros(2);
    
    stateNum = classify(state);
    for i in range(len(actions)):
        stateActionNum = stateNum + i*STATES;
        actionPreference[i] = weights[stateActionNum];
    
    actionProbabilities = []
    aggr = 0;
    
    for i in range(len(actions)):
        actionProbabilities.append(np.exp(np.minimum(4.0, np.maximum(-4.0, actionPreference[i]))));
        aggr += actionProbabilities[i];
    
    
    actionProbabilities = [i/aggr for i in actionProbabilities];

    if np.abs(np.sum(actionProbabilities) - 1) > 1e-8:
        print("Action probabilities greater than 1");
    
    return actionProbabilities

def getNextAction(state, weights):
    actions = [0,1];
    
    actionProbabilities = policyProbability(state,weights)
    
    randNum = random.random();
    
    if randNum < actionProbabilities[0]:
        return actions[0];
    else:
        return actions[1];

def getStateValue(state, stateWeights):
    stateNum = classify(state);
    if stateNum == -1:
        return 0;
    else:
        return stateWeights[stateNum];

def getStateValueGrad(state,length):
    stateNum = classify(state);
    grad = np.zeros(length);
    grad[stateNum] = 1;
    return grad;

def getPolicyGrad(action,state,weights):
    actions = [0,1];
    grad = np.zeros(len(weights));
    stateNum = classify(state)
    actionProbabilities = policyProbability(state,weights);
    
    for i in actions:
        grad[stateNum + STATES*i] = -actionProbabilities[i];
        if i == action:
            grad[stateNum + STATES*i] += 1

    return grad



a = 1e-4;
gamma = 0.99;
weights = np.zeros(STATES*2);

env = gym.make('CartPole-v0');



for i_episode in range(3000):
    states = [];
    actions = [];
    rewards = [];
    observation = env.reset()

    states.append(observation);
    
    for t in range(1000):
        #env.render()
        #/print(observation)
        
        action = getNextAction(observation, weights);
        observation, reward, done, info = env.step(action);
        
        actions.append(action);
        rewards.append(reward);
        states.append(observation);
        
        if (done):
            for step in range(t):
                g = sum(rewards[step:]);
                weights = weights + a * gamma**t * g * getPolicyGrad(actions[step],states[step],weights);
            
            print("Episode finished after {} timesteps".format(t+1))
            break

observation = env.reset()
for t in range(1000):
    env.render()
    action = getNextAction(observation,weights);
    observation, reward, done, info = env.step(action);
    if done:
        print("Episode finished after {} timesteps".format(t+1))
        break

