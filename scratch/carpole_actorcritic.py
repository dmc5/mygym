import gym
import numpy as np
import random
import sys
import time as time_lib
import matplotlib
matplotlib.use('TKAgg');
import matplotlib.pyplot as plt

env = gym.make('CartPole-v0');

a = 1e-1;
#b = 1e-3;
b = 1e-4;
gamma = 0.95;

input_dim = 4;
hidden_dim = 300;
hidden_value_dim = 300;
output_dim = 1;

#Initialize Model
W1 = np.random.randn(hidden_dim,input_dim) / np.sqrt(input_dim) # "Xavier" initialization
W2 = np.random.randn(hidden_dim) / np.sqrt(hidden_dim)

STATE1 = np.random.randn(hidden_value_dim,input_dim) / np.sqrt(input_dim)  # "Xavier" initialization
STATE2 = np.random.randn(hidden_value_dim) / np.sqrt(hidden_dim)


def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def policyForward(x):
    h = np.dot(W1,x);
    h[h<0] = 0;
    out = np.dot(W2, h);
    prob = sigmoid(out);
    return prob, h

def stateValueForward(x):
    h = np.dot(STATE1,x);
    #h[h<0] = 0;
    h = sigmoid(h);
    out = np.dot(STATE2, h);
    return out, h

def backProp(y, y_net, x, h1, W1_, W2_):
    delta_in_output = (y - y_net); # * (y_net * (1.0-y_net));
    
    h1_temp = np.copy(h1);
    h1_temp[h1_temp<0] = 0;
    h1_temp[h1_temp>0] = 1;
    
    delta_in_hidden = (delta_in_output * W2_) * h1_temp;
    
    return np.outer(delta_in_hidden, x), np.outer(delta_in_output, h1);

def stateBackProp(y, y_net, x, h1, W1_, W2_):
    delta_in_output = (y - y_net); # * (y_net * (1.0-y_net));
    
    delta_in_hidden = (delta_in_output * W2_) * (h1 * (1.0 - h1));
    
    return np.outer(delta_in_hidden, x), np.outer(delta_in_output, h1);


plot = False
for i in range(30):
    I = 1;
    observation = env.reset();
    
    observation = observation / np.linalg.norm(observation);

    new_obs = observation;
    
    for t in range(202):
        #env.render()

        prob, hidden = policyForward(observation);
        value, hidden_value = stateValueForward(observation); # the expected return for the state
        
        action = 1 if prob > random.random() else 0;

        new_obs, reward, done, info = env.step(action);
        new_obs = new_obs / np.linalg.norm(new_obs);
        
        if (done):
            next_value = np.array([0]);
        else:
            next_value, _dummy = stateValueForward(new_obs) # the expected return for the next state
        
        #reward + gamma*next_value is the more accurate value for the expected return for the next state
        delta = reward + gamma * next_value - value;
    
        
#        stateGrad1, stateGrad2   = backProp(reward + gamma * next_value,
#                                            value,
#                                            observation,
#                                            hidden_value,
#                                            STATE1, STATE2);

        for f in range(10):
            stateGrad1, stateGrad2   = stateBackProp(reward + gamma * next_value,
                                                value,
                                                observation,
                                                hidden_value,
                                                STATE1, STATE2);
            STATE1 = STATE1 + b * abs(delta) * stateGrad1;
            STATE2 = STATE2 + b * abs(delta) * stateGrad2;
        
        
        policyGrad1, policyGrad2 = backProp(action,
                                            prob,
                                            observation,
                                            hidden,
                                            W1, W2);
                                            
        W1 = W1 + a * delta * I * policyGrad1;
        W2 = W2 + a * delta * I * policyGrad2;
    
        
        foo, bar = stateValueForward(observation);
        if (i == 5 or i == 29):
            print("Iteration {0}, step {1},\t Goal {2}, \t new {6} \t old {3},\t error {4},\t change {5}".format(i,t,reward + gamma * next_value, value, reward+gamma*next_value - value, foo - value, foo));





        
        observation = new_obs;
        I = I * gamma
        
        if (done):
            if (i % 1 == 0):
                print("Episode {0} finished after {1} timesteps".format(i, t+1))
            if (t > 10):
                plot = True;
                x_vals = np.arange(-1,1,0.001);
                y_vals = np.zeros(np.size(x_vals));
                for p in range(len(x_vals)):
                    state = np.array([0, x_vals[p], 0, 0]);
                    y_val, _dummy = stateValueForward(state);
                    y_vals[p] = y_val;
    
            break
if plot:
    plt.plot(x_vals, y_vals)
    plt.show()
#observation = env.reset()
#for t in range(1000):
#    env.render()
#    action = getNextAction(observation,weights);
#    observation, reward, done, info = env.step(action);
#    if done:
#        print("Episode finished after {} timesteps".format(t+1))
#        break

