import numpy as np
import random

input_dim = 4;
hidden_dim = 10;
hidden_dim_2 = 20;
output_dim = 1;

#Initialize Model
W1 = np.random.randn(hidden_dim,input_dim) / np.sqrt(input_dim) # "Xavier" initialization
W2 = np.random.randn(hidden_dim_2,hidden_dim) / np.sqrt(hidden_dim)
W3 = np.random.randn(hidden_dim_2) / np.sqrt(hidden_dim_2)


def fun1(x):
#    return np.exp(-1 * np.linalg.norm(x, axis = 0))
    return 1.0 / np.linalg.norm(x,axis=0);

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def policyForward(x):
    h1 = np.dot(W1,x);
    h1[h1<0] = 0;
    h2 = np.dot(W2, h1);
    h2[h2<0] = 0;
    out = np.dot(W3, h2);
    prob = sigmoid(out);
    return prob, h1, h2

def total_error(X,Y):
    Y_prime, _dummy, _dummy2 = policyForward(X);
    return np.linalg.norm(Y_prime-Y), np.max(np.abs(Y_prime-Y));

def backProp(y, y_net, x, h1, h2, W1_, W2_, W3_):
    delta_in_output = (y - y_net) * (y_net * (1.0-y_net));
    #delta_in_hidden = (delta_in_output * W2_) * (hidden * (1.0 - hidden));
    
    
    print(np.shape(h2));
    h2_temp = np.copy(h2);
    h2_temp[h2_temp<0] = 0;
    h2_temp[h2_temp>0] = 1;
    delta_in_hidden2 = (delta_in_output * W3_) * h2_temp;
    
    print(np.shape(delta_in_hidden2));
    
    h1_temp = np.copy(h1);
    h1_temp[h1_temp<0] = 0;
    h1_temp[h1_temp>0] = 1;
    delta_in_hidden1 = (delta_in_hidden2, W2_) * h1_temp;
    
    
    W3_ = W3_ + 0.5 * np.outer(delta_in_output, h2);
    W2_ = W2_ + 0.5 * np.outer(delta_in_hidden2, h1);
    W1_ = W1_ + 0.5 * np.outer(delta_in_hidden1, x);
    
    return W1_, W2_, W3_;


#Generate Data
data_size = 1000;
X = np.random.randn(input_dim, data_size) * 1;
Y = fun1(X);
print(total_error(X,Y));

for i in range(data_size):
    x = X[:,i];
    y = Y[i];
    y_net, h1, h2 = policyForward(x);
    W1, W2, W3 = backProp(y, y_net, x, h1, h2, W1, W2, W3);

print(total_error(X,Y));

