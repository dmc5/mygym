import gym
import numpy as np
import random


barriers = [-2.4,-2.0 -1.0, 1.0, 2.0, 2.4];
barriersDot = [-0.5, -.01, .01, 0.5, float('inf')];
barriers2 = [-np.pi/15.0, -np.pi/30.0, -np.pi/45.0, -np.pi/180.0, np.pi/180.0, np.pi/45.0, np.pi/30, np.pi/15.0];
barriers2Dot = [-np.pi/45, -np.pi/90.0, -np.pi/180, np.pi/180.0, np.pi/90.0,np.pi/45.0, float('inf')];
STATES = (len(barriers)-1)*(len(barriers2)-1)*len(barriersDot)*len(barriers2Dot);


def classify(state):
    """
    Takes in the state (car position, car velocity, pole angle, pole angular velocity)
    and returns the state number of the system
    """
    num = 0;
        #if state[0] < barriers[0] or state[0] > barriers[-1] or state[2] < barriers2[0] or state[2] > barriers2[-1]:
        #    return -1;
    
    #Tile car position
    for i in range(len(barriers)-1):
       if state[0] < barriers[i+1]:
           num = i
           break;

    #Tile for car velocity
    for i in range(len(barriersDot)-1):
       if state[1] < barriersDot[i+1]:
           num += i*(len(barriers)-1);
           break;
    
    #Tile for pole angle
    for i in range(len(barriers2)):
        if state[2] < barriers2[i]:
            num += (i)*(len(barriersDot)-1)*(len(barriers)-1);
            break;

    #Tile for theta2dot
    for i in range(len(barriers2Dot)):
        if state[3] < barriers2Dot[i]:
            num += (i) * (len(barriersDot)) * (len(barriers2)-1)*(len(barriers)-1);
            break;
    
    return num

def policyProbability(state,weights):
    actions = [0,1];
    actionPreference = np.zeros(2);
    
    stateNum = classify(state);
    for i in range(len(actions)):
        stateActionNum = stateNum + i*STATES;
        actionPreference[i] = weights[stateActionNum];
    
    actionProbabilities = []
    aggr = 0;
    
    for i in range(len(actions)):
        actionProbabilities.append(np.exp(np.minimum(4.0, np.maximum(-4.0, actionPreference[i]))));
        aggr += actionProbabilities[i];
    
    
    actionProbabilities = [i/aggr for i in actionProbabilities];

    if np.abs(np.sum(actionProbabilities) - 1) > 1e-8:
        print("Action probabilities greater than 1");
    
    return actionProbabilities

def getNextAction(state, weights):
    actions = [0,1];
    
    actionProbabilities = policyProbability(state,weights)
    
    randNum = random.random();
    
    if randNum < actionProbabilities[0]:
        return actions[0];
    else:
        return actions[1];

def getStateValue(state, stateWeights):
    stateNum = classify(state);
    if stateNum == -1:
        return 0;
    else:
        return stateWeights[stateNum];

def getStateValueGrad(state,length):
    stateNum = classify(state);
    grad = np.zeros(length);
    grad[stateNum] = 1;
    return grad;

def getPolicyGrad(action,state,weights):
    actions = [0,1];
    grad = np.zeros(len(weights));
    stateNum = classify(state)
    actionProbabilities = policyProbability(state,weights);
    
    for i in actions:
        grad[stateNum + STATES*i] = -actionProbabilities[i];
        if i == action:
            grad[stateNum + STATES*i] += 1

    return grad



lambdaTh = 0.9;
lambdaSt = 0.8;
a = 1;
b = 0.5;
gamma = 0.95;
weights = np.zeros(STATES*2);
stateWeights = np.zeros(STATES);

env = gym.make('CartPole-v0')

for i_episode in range(300):
    eTheta = np.zeros(STATES*2);
    eState = np.zeros(STATES);
    I = 1;
    observation = env.reset()
    pre_obs = observation;
    for t in range(1000):
        #env.render()
        #/print(observation)
        
        
        action = getNextAction(observation,weights);
        
        observation, reward, done, info = env.step(action);
        
        if (done):
            reward = -100;
            delta = reward - getStateValue(pre_obs, stateWeights);
        else:
            delta = reward + gamma*getStateValue(observation,stateWeights) - getStateValue(pre_obs, stateWeights);
            
        eState = lambdaSt * eState + I * getStateValueGrad(pre_obs, STATES);
            
        eTheta = lambdaTh * eTheta + I * getPolicyGrad(action, pre_obs, weights);
        
            
        stateWeights = stateWeights + b*delta*eState;
            
        weights = weights + a*delta*eTheta;
            
        I = gamma*I;
            
        pre_obs = observation;
        
        if done:
            print("Episode finished after {} timesteps".format(t+1))
            break

I = 1;
observation = env.reset()
pre_obs = observation;
for t in range(1000):
    env.render()
    #/print(observation)
    
    
    action = getNextAction(observation,weights);
    
    observation, reward, done, info = env.step(action);
    
    if (done):
        reward = -100;
        delta = reward - getStateValue(pre_obs, stateWeights);
    else:
        delta = reward + gamma*getStateValue(observation,stateWeights) - getStateValue(pre_obs, stateWeights);

    #eState = lambdaSt * eState + I * getStateValueGrad(observation, STATES);
    
    #eTheta = lambdaTh * eTheta + I * getPolicyGrad(action, observation, weights);
    
    stateWeights = stateWeights + b*delta*getStateValueGrad(pre_obs,STATES);
    
    weights = weights + a*I*delta*getPolicyGrad(action,pre_obs,weights);
    
    I = gamma*I;
    
    pre_obs = observation;
    
    if done:
        print("Episode finished after {} timesteps".format(t+1))
        break

