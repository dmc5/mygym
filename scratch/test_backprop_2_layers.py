import numpy as np
import random

input_dim = 4;
hidden_dim = 300;
output_dim = 1;

#Initialize Model
W1 = np.random.randn(hidden_dim,input_dim) / np.sqrt(input_dim) # "Xavier" initialization
W2 = np.random.randn(hidden_dim) / np.sqrt(hidden_dim)


def fun1(x):
#    return np.exp(-1 * np.linalg.norm(x, axis = 0))
#    return 1.0 / np.linalg.norm(x,axis=0);
    return -1 * x[0] ** 2

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def policyForward(x):
    h1 = np.dot(W1,x);
    h1[h1<0] = 0;
    out = np.dot(W2, h1);
    #prob = sigmoid(out);
    return out, h1

def total_error(X,Y):
    Y_prime, _dummy = policyForward(X);
    return np.linalg.norm(Y_prime-Y), np.max(np.abs(Y_prime-Y));

def backProp(y, y_net, x, h1, W1_, W2_):
    delta_in_output = (y - y_net) #* (y_net * (1.0-y_net));
    #delta_in_hidden = (delta_in_output * W2_) * (hidden * (1.0 - hidden));
    h1_temp = np.copy(h1);
    h1_temp[h1_temp<0] = 0;
    h1_temp[h1_temp>0] = 1;
    
    delta_in_hidden = (delta_in_output * W2_) * h1_temp;
    
    W2_ = W2_ + 0.015 * np.outer(delta_in_output, h1);
    W1_ = W1_ + 0.015 * np.outer(delta_in_hidden, x);
    
    return W1_, W2_;


#Generate Data
data_size = 1000;
X = np.random.randn(input_dim, data_size);
Y = fun1(X);
print("Before", total_error(X,Y));

for j in range(10):
    for i in range(data_size):
        x = X[:,i];
        y = Y[i];
        y_net, h1 = policyForward(x);
        W1, W2 = backProp(y, y_net, x, h1, W1, W2);

print("After {0} loops Error {1}".format(j, total_error(X,Y)));

