import gym
import numpy as np
import random
import sys
import time as time_lib

env = gym.make('CartPole-v0');

a = 1e-2;
gamma = 0.95;

input_dim = 4;
hidden_dim = 300;
output_dim = 1;

#Initialize Model
W1 = np.random.randn(hidden_dim,input_dim) / np.sqrt(input_dim) # "Xavier" initialization
W2 = np.random.randn(hidden_dim) / np.sqrt(hidden_dim)

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def policyForward(x):
    h = np.dot(W1,x);
    h[h<0] = 0;
    out = np.dot(W2, h);
    prob = sigmoid(out);
    return prob, h

def backProp(y, y_net, x, h1, W1_, W2_):
    delta_in_output = (y - y_net);# * (y_net * (1.0-y_net));
    
    h1_temp = np.copy(h1);
    h1_temp[h1_temp<0] = 0;
    h1_temp[h1_temp>0] = 1;
    
    delta_in_hidden = (delta_in_output * W2_) * h1_temp;
    
    return np.outer(delta_in_hidden, x), np.outer(delta_in_output, h1);



update_speed = 0.5;
def update_average(time, time_average, i):
    time_average = (1 - update_speed) * time_average + update_speed * time;
    text = "\rIteration {0} \t Average: {1}".format(i, time_average);
    sys.stdout.write(text)
    sys.stdout.flush()
    return time_average;




time_average = 0;
average_reward = 0;
for i in range(200000):
    states = [];
    probs = [];
    hiddens = [];
    actions = [];
    rewards = [];
    observation = env.reset();
    states.append(observation);
    for t in range(1000):
        #env.render()
        
        prob, h = policyForward(observation);
        action = 1 if prob > random.random() else 0;

        observation, reward, done, info = env.step(action);
        
        actions.append(action);
        hiddens.append(h);
        probs.append(prob);
        rewards.append(reward);
        states.append(observation);
        
        if (done):
            for step in range(t):
                g = sum(rewards[step:]);  #need to discount
                
                average_reward = 0.9*average_reward + 0.1 * g;
                
                delta1, delta2 = backProp(actions[step],
                                         probs[step],
                                         states[step],
                                         hiddens[step],
                                         W1, W2);
                W1 = W1 + a * (g-average_reward) * gamma ** step * delta1;
                W2 = W2 + a * (g-average_reward) * gamma ** step * delta2;
            if (i % 10 == 0):
                print("Episode {2} finished after {0} timesteps. Average reward {1}".format(t+1, average_reward, i))
            break


#observation = env.reset()
#for t in range(1000):
#    env.render()
#    action = getNextAction(observation,weights);
#    observation, reward, done, info = env.step(action);
#    if done:
#        print("Episode finished after {} timesteps".format(t+1))
#        break

