import gym
import numpy as np
import random
import sys
import time as time_lib

env = gym.make('CartPole-v0');

a = 1e-3;
b = 1e-5;
gamma = 0.95;

input_dim = 4;
hidden_dim = 300;
output_dim = 1;

#Initialize Model
W1 = np.random.randn(hidden_dim,input_dim) / np.sqrt(input_dim) # "Xavier" initialization
W2 = np.random.randn(hidden_dim) / np.sqrt(hidden_dim)

STATE1 = np.random.randn(hidden_dim,input_dim) / np.sqrt(input_dim) # "Xavier" initialization
STATE2 = np.random.randn(hidden_dim) / np.sqrt(hidden_dim)


def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def policyForward(x):
    h = np.dot(W1,x);
    h[h<0] = 0;
    out = np.dot(W2, h);
    prob = sigmoid(out);
    return prob, h

def stateValueForward(x):
    h = np.dot(STATE1,x);
    h[h<0] = 0;
    out = np.dot(STATE2, h);
    return out, h

def backProp(y, y_net, x, h1, W1_, W2_):
    delta_in_output = (y - y_net); # * (y_net * (1.0-y_net));
    
    h1_temp = np.copy(h1);
    h1_temp[h1_temp<0] = 0;
    h1_temp[h1_temp>0] = 1;
    
    delta_in_hidden = (delta_in_output * W2_) * h1_temp;
    
    return np.outer(delta_in_hidden, x), np.outer(delta_in_output, h1);


for i in range(300):
    states = [];
    probs = [];
    hiddens = [];
    actions = [];
    rewards = [];
    values = [];
    hidden_values = [];
    observation = env.reset();
    observation = observation / np.linalg.norm(observation);
    states.append(observation);
    for t in range(1000):
        #env.render()
        
        prob, h = policyForward(observation);
        value, hidden_value = stateValueForward(observation);
        
        action = 1 if prob > random.random() else 0;

        observation, reward, done, info = env.step(action);
        observation = observation / np.linalg.norm(observation);
        
        actions.append(action);
        hiddens.append(h);
        probs.append(prob);
        rewards.append(reward);
        states.append(observation);
        values.append(value);
        hidden_values.append(hidden_value);
        
        if (done):
            for step in range(t):
                g = sum(rewards[step:]);  #need to discount
                
                delta = g - values[step];
                
                policyGrad1, policyGrad2 = backProp(actions[step],
                                                    probs[step],
                                                    states[step],
                                                    hiddens[step],
                                                    W1, W2);

#                stateGrad1, stateGrad2   = backProp(actions[step],
#                                                    probs[step],
#                                                    states[step],
#                                                    hidden_values[step],
#                                                    STATE1, STATE2);

                stateGrad1, stateGrad2   = backProp(g,
                                                    values[step],
                                                    states[step],
                                                    hidden_values[step],
                                                    STATE1, STATE2);
                                                    
                W1 = W1 + a * delta * gamma ** step * policyGrad1;
                W2 = W2 + a * delta * gamma ** step * policyGrad2;
            
            
                STATE1 = STATE1 + b * stateGrad1;
                STATE2 = STATE2 + b * stateGrad2;
#                foo, bar = stateValueForward(states[step]);
#                if (abs(delta) < abs(g-foo)):
#                    print("Iteration {0}, step {1}, moved in wrong direction: {2} ---> {3}".format(i,step,delta,g-foo));
#                    print("\t Goal: {0}, Old estimate {1}, New estimate {2}".format(g, values[step],foo));
#                    print("\n")
            
            
            
            if (i % 10 == 0):
                print("Episode {0} finished after {1} timesteps".format(i, t+1))
            break


#observation = env.reset()
#foo, bar = stateValueForward(observation);
#print(foo);
#print(STATE2);
#for t in range(1000):
#    env.render()
#    action = getNextAction(observation,weights);
#    observation, reward, done, info = env.step(action);
#    if done:
#        print("Episode finished after {} timesteps".format(t+1))
#        break

